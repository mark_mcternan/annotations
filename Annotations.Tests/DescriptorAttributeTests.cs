﻿// -----------------------------------------------------------------------
// <copyright file="DescriptorAttributeTests.cs" company="Datus LLC">
// Copyright (c) "Datus LLC". All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Datus.Annotations.Tests
{
    using System;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class DescriptorAttributeTests
    {
        [TestMethod]
        [Priority(3)]
        [TestCategory(TestCategories.UnitTest)]
        public void GetDescriptors_WithDecoratedEnum_ReturnsListOfDescriptors()
        {
            // Arrange
            var x = new DescripterDecoratedTestEnum();

            // Act
            var actual = new List<string>(x.GetDescriptors());

            // Assert
            Assert.IsTrue(actual.Count > 0, "Descriptor count is zero and shouldn't be");
            Assert.AreEqual(5, actual.Count, $"Descriptor count was {actual.Count} and should have been 5");
        }

        [TestMethod]
        [Priority(3)]
        [TestCategory(TestCategories.UnitTest)]
        public void GetDescriptor_WithUndecoratedValue_ReturnsNull()
        {
            // Arrange
            var x = DescripterDecoratedTestEnum.ItGoesBoom;

            // Act
            var actual = x.GetDescriptor();

            // Assert
            Assert.IsNull(actual, "GetDescriptor returned non null response with an undecorated enumeration");
        }

        [TestMethod]
        [Priority(3)]
        [TestCategory(TestCategories.UnitTest)]
        public void GetDescriptor_WithDecoratedValue_ReturnsDescriptor()
        {
            // Arrange
            var x = DescripterDecoratedTestEnum.NotFound;

            // Act
            var actual = x.GetDescriptor();

            // Assert
            Assert.IsNotNull(actual, "GetDescriptor returned null value with a decorated enumeration");
            Assert.AreEqual<string>("File not found", actual, $"GetDescriptor returned unexpected '{actual}' descriptor with a decorated enumeration");
        }

        [TestMethod]
        [Priority(3)]
        [TestCategory(TestCategories.UnitTest)]
        public void GetDescriptor_WithInvalidEnumeration_ReturnsNull()
        {
            // Arrange
            var x = DescripterDecoratedTestEnum.NotFound + 23; // 427

            // Act
            var actual = x.GetDescriptor();

            // Assert
            Assert.IsNull(actual, $"GetDescriptor returned non-null value of '{actual}' with an invalid enumeration");
        }

        [TestMethod]
        [Priority(3)]
        [TestCategory(TestCategories.UnitTest)]
        public void GetDescriptor_WithMultipleDecoratedValues_ReturnsNull()
        {
            // Arrange
            var x = AnotherDecoratedTestEnum.Condition1 | AnotherDecoratedTestEnum.Condition4;

            // Act
            var actual = x.GetDescriptor();

            // Assert
            Assert.IsNotNull(actual, "GetDescriptor returned null value with a decorated enumeration");
        }
    }
}
