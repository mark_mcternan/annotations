﻿// -----------------------------------------------------------------------
// <copyright file="TestCategories.cs" company="Datus LLC">
// Copyright (c) "Datus LLC". All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Datus.Annotations.Tests
{
    using System;

    public struct TestCategories
    {
        public const string UnitTest = "Unit Test";
        public const string IntegrationTest = "Integration Test";
    }
}
