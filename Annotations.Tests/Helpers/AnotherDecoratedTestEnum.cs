﻿// -----------------------------------------------------------------------
// <copyright file="TestCategories.cs" company="Datus LLC">
// Copyright (c) "Datus LLC". All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Datus.Annotations.Tests
{
    using System;

    [Flags]
    public enum AnotherDecoratedTestEnum
    {
        [Descriptor("Condition #1")]
        Condition1 = 1,
        [Descriptor("Condition #2")]
        Condition2 = 2,
        [Descriptor("Condition #4")]
        Condition4 = 4,
        [Descriptor("Condition #8")]
        Condition8 = 8
    }
}
