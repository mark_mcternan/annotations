﻿// -----------------------------------------------------------------------
// <copyright file="TestCategories.cs" company="Datus LLC">
// Copyright (c) "Datus LLC". All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Datus.Annotations.Tests
{
    using System;
    using Datus.Annotations;

    [Flags]
    public enum DescripterDecoratedTestEnum
    {
        [Descriptor("OK")]
        Ok = 200,
        [Descriptor("File not found")]
        NotFound = 404,
        [Descriptor("Unauthenticated")]
        Unauthorized = 401,
        [Descriptor("Forbidden")]
        Forbidden = 403,
        [Descriptor("An internal server error occcurred.")]
        ServerError = 500,
        ItGoesBoom = 700
    }
}
