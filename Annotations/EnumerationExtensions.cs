﻿// -----------------------------------------------------------------------
// <copyright file="EnumerationExtensions.cs" company="Datus LLC">
// Copyright (c) "Datus LLC". All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Datus.Annotations
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Linq;

    /// <summary>
    /// Extension Class for Enumerations that supports the DescriptorAttribute
    /// </summary>
    public static class EnumerationExtensions
    {
        /// <summary>
        /// Gets any matched descriptor for a given enumerated constant
        /// </summary>
        /// <param name="enumeration">the enumerated constant</param>
        /// <returns>the descriptor as string or null if unmatched or unset</returns>
        public static string GetDescriptor(this Enum enumeration)
        {
            var type = enumeration.GetType();

            // singular matched constant
            if(Enum.IsDefined(type, enumeration))
            {
                return type.GetRuntimeField(enumeration.ToString())?.GetCustomAttribute<DescriptorAttribute>()?.Description;
            }

            // multi-valued, flagged enum handling
            if(type.GetTypeInfo().GetCustomAttribute<FlagsAttribute>() != null)
            {
                var descriptors = new List<string>();
                foreach(var name in Enum.GetNames(type))
                {
                    var constant = Enum.Parse(type, name) as Enum;
                    if (enumeration.HasFlag(constant) && constant.GetDescriptor() != null)
                    {
                        descriptors.Add(constant.GetDescriptor());
                    }
                }

                return descriptors.Count > 0 ? String.Join(" | ", descriptors.ToArray()) : null;
            }

            return null;
        }

        /// <summary>
        /// Gets the set of Descriptors for a given Enumeration
        /// </summary>
        /// <param name="enumeration">the enumerated constant</param>
        /// <returns>an enumerable set of descriptors</returns>
        public static IEnumerable<string> GetDescriptors(this Enum enumeration)
        {
            var type = enumeration.GetType();

            return from field in type.GetRuntimeFields()
                   where field.GetCustomAttribute<DescriptorAttribute>() != null
                   select field.GetCustomAttribute<DescriptorAttribute>().Description;
        }
    }
}
