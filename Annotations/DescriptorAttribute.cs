﻿// -----------------------------------------------------------------------
// <copyright file="DescriptorAttribute.cs" company="Datus LLC">
// Copyright (c) "Datus LLC". All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Datus.Annotations
{
    using System;

    /// <summary>
    /// DescriptorAttribute class
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple=false)]
	public class DescriptorAttribute : Attribute
	{
		private readonly string _descriptor;

		/// <summary>
		/// Initializes an instance of the DescriptorAttribute class.
		/// </summary>
		/// <param name="description">The Description to associate with an enumerated constant</param>
		public DescriptorAttribute(string description)
		{
			_descriptor = description;
		}

		/// <summary>
		/// Gets the associated description attribute for an Enumerated Value of field
		/// </summary>
		public string Description
		{ 
			get { return _descriptor; }
		}
	}
}
